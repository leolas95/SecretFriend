# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171217203036) do
  create_table "admins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["user_id"], name: "index_admins_on_user_id"
  end

  create_table "buys", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "participant_id", null: false
    t.bigint "gift_id", null: false
    t.date "date_of_purchase", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gift_id"], name: "index_buys_on_gift_id"
    t.index ["participant_id", "gift_id"], name: "index_buys_on_participant_id_and_gift_id", unique: true
    t.index ["participant_id"], name: "index_buys_on_participant_id"
  end

  create_table "exchanges", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "exchange_date", null: false
    t.text "place", null: false
    t.bigint "group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["exchange_date", "group_id"], name: "index_exchanges_on_exchange_date_and_group_id", unique: true
    t.index ["group_id"], name: "index_exchanges_on_group_id"
  end

  create_table "gifts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "code", null: false
    t.bigint "shop_id", null: false
    t.string "name", null: false
    t.float "price", limit: 24, null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code", "shop_id"], name: "index_gifts_on_code_and_shop_id", unique: true
    t.index ["shop_id"], name: "index_gifts_on_shop_id"
  end

  create_table "gives", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "giver_id", null: false
    t.bigint "receiver_id", null: false
    t.bigint "exchange_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "giver_calification", limit: 24
    t.index ["exchange_id"], name: "index_gives_on_exchange_id"
    t.index ["giver_id", "receiver_id", "exchange_id"], name: "index_gives_on_giver_id_and_receiver_id_and_exchange_id", unique: true
    t.index ["giver_id"], name: "index_gives_on_giver_id"
    t.index ["receiver_id"], name: "index_gives_on_receiver_id"
  end

  create_table "groups", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.string "admin_email", null: false
    t.bigint "admin_id", null: false
    t.integer "max_participants_allowed", null: false
    t.float "min_gift_price", limit: 24
    t.float "max_gift_price", limit: 24
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_groups_on_admin_id"
    t.index ["name", "admin_email"], name: "index_groups_on_name_and_admin_email", unique: true
  end

  create_table "likes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id", null: false
    t.string "user_like", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id", "user_like"], name: "index_likes_on_user_id_and_user_like", unique: true
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "migration_validators", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "table_name", null: false
    t.string "column_name", null: false
    t.string "validation_type", null: false
    t.string "options"
    t.index ["table_name", "column_name", "validation_type"], name: "unique_idx_on_migration_validators"
  end

  create_table "participants", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_participants_on_email", unique: true
    t.index ["user_id"], name: "index_participants_on_user_id"
  end

  create_table "participates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "participant_id", null: false
    t.bigint "group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_participates_on_group_id"
    t.index ["participant_id"], name: "index_participates_on_participant_id"
  end

  create_table "shops", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.text "address", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", null: false
    t.string "name", null: false
    t.string "lastname", null: false
    t.string "gender"
    t.float "avgscore", limit: 24, default: 0.0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.date "birthday", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "wishes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "participate_id", null: false
    t.string "participant_wish", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["participate_id", "participant_wish"], name: "index_wishes_on_participate_id_and_participant_wish", unique: true
    t.index ["participate_id"], name: "index_wishes_on_participate_id"
  end

  add_foreign_key "admins", "users", on_delete: :cascade
  add_foreign_key "buys", "gifts", on_delete: :cascade
  add_foreign_key "buys", "participants", on_delete: :cascade
  add_foreign_key "exchanges", "groups", on_delete: :cascade
  add_foreign_key "gifts", "shops", on_delete: :cascade
  add_foreign_key "gives", "exchanges", on_delete: :cascade
  add_foreign_key "groups", "admins", on_delete: :cascade
  add_foreign_key "likes", "users", on_delete: :cascade
  add_foreign_key "participants", "users", on_delete: :cascade
  add_foreign_key "participates", "groups", on_delete: :cascade
  add_foreign_key "participates", "participants", on_delete: :cascade
  add_foreign_key "wishes", "participates", on_delete: :cascade
end
