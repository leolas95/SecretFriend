# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

gustos1 = [
  "Libros", "Peliculas", "Juegos de mesa", "Dulces", "Zapatos",
  "Electronica", "Musica", "Matematica",
]

gustos2 = [
  "Musica clasica", "Chocolate oscuro", "Filosofia", "Deportes", "Futbol",
  "Natacion", "Autos", "Manualidades",
]

gustos3 = [
  "Bicicross", "Beisbol", "Tenis", "Boxeo", "Mascotas", "Hiking",
  "Paracaidismo", "Fotografia",
]

u = User.create!(email: "leo@gmail.com",
                 name: "Pedro",
                 lastname: "Perez",
                 birthday: 22.years.ago,
                 gender: "Male",
                 password: "abc123",
                 password_confirmation: "abc123")

Like.create!(user_id: u.id, user_like: gustos1.sample)
Like.create!(user_id: u.id, user_like: gustos2.sample)
Like.create!(user_id: u.id, user_like: gustos3.sample)

a = Admin.create!(email: u.email, user_id: u.id)

# Crea los usuarios de prueba
20.times do |n|
  email = "usuario#{n + 1}@gmail.com"
  name = Faker::Name.first_name
  lastname = Faker::Name.last_name
  birthday = 25.years.ago + n.years
  gender = n.even? ? "Male" : "Female"
  password = "123456"
  us = User.create!(email: email,
                    name: name,
                    lastname: lastname,
                    birthday: birthday,
                    gender: gender,
                    password: password,
                    password_confirmation: password)

  Like.create!(user_id: us.id, user_like: gustos1.sample)
  Like.create!(user_id: us.id, user_like: gustos2.sample)
  Like.create!(user_id: us.id, user_like: gustos3.sample)
end

Group.create!(
  name: "El grupo", admin_email: a.email,
  admin_id: a.id, max_participants_allowed: 10,
  min_gift_price: 10,
)

precios = [123.456, 789.987, 145.23, 657.54, 8.23, 87.45, 12.43, 9.23, 6.58,
           2.14, 24.56, 45.66, 44.23, 67.89, 32.44, 21.33, 19.90, 17.72, 45.56]

descripciones = [
  "Una buena opcion para personas deportivas",
  "Perfecto para quienes gustan de leer",
  "Para amantes de la cocina, la mejor opcion",
  "Si te gustan los deportes extremos, esto te fascinara",
  "Para personas atrevidas que les gusten los deportes de alto rendimiento",
  "Para quienes les gustan las peliculas",
  "Este regalo es para personas con gustos de musica",
  "Esto le gustara a los amantes de las peliculas",
  "Rico dulce para quienes les gusta el chocolate",
  "Para aquellos que les gusten los chocolates",
]

# Se crean 6 tiendas, con 3 regalos cada una

s1 = Shop.create!(name: "Kyoto", address: "Centro Comercial Alta Vista, piso 1, local 303")
Gift.create!(code: Faker::Crypto.md5, shop_id: s1.id, name: gustos1.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s1.id, name: gustos2.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s1.id, name: gustos3.sample,
             price: precios.sample, description: descripciones.sample)

s2 = Shop.create!(name: "Adidas", address: "Centro Comercial Orinokia Mall, seccion Acero, local 123")
Gift.create!(code: Faker::Crypto.md5, shop_id: s2.id, name: gustos1.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s2.id, name: gustos2.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s2.id, name: gustos3.sample,
             price: precios.sample, description: descripciones.sample)

s3 = Shop.create!(name: "Tony Romas", address: "Centro Comercial Orinokia Mall, seccion Madera, local 456")
Gift.create!(code: Faker::Crypto.md5, shop_id: s3.id, name: gustos1.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s3.id, name: gustos2.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s3.id, name: gustos3.sample,
             price: precios.sample, description: descripciones.sample)

s4 = Shop.create!(name: "Todo electronica", address: "Centro Comercial Alta Vista, piso 1, local 304, al lado de Kyoto")
Gift.create!(code: Faker::Crypto.md5, shop_id: s4.id, name: gustos1.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s4.id, name: gustos2.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s4.id, name: gustos3.sample,
             price: precios.sample, description: descripciones.sample)

s5 = Shop.create!(name: "Tattoo", address: "Centro Comercial Alta Vista, piso 1, local 305, al frente de Todo electronica")
Gift.create!(code: Faker::Crypto.md5, shop_id: s5.id, name: gustos1.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s5.id, name: gustos2.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s5.id, name: gustos3.sample,
             price: precios.sample, description: descripciones.sample)

s6 = Shop.create!(name: "Libreria latina", address: "Centro Comercial Orinokia Mall, seccion Aluminio, local 789")
Gift.create!(code: Faker::Crypto.md5, shop_id: s6.id, name: gustos1.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s6.id, name: gustos2.sample,
             price: precios.sample, description: descripciones.sample)

Gift.create!(code: Faker::Crypto.md5, shop_id: s6.id, name: gustos3.sample,
             price: precios.sample, description: descripciones.sample)
