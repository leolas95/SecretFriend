class AddWishToIndexToParticipates < ActiveRecord::Migration[5.1]
  def change
		remove_index :participates, name: "index_participates_on_participant_id_and_group_id" if index_exists?(:participates, "index_participates_on_participant_id_and_group_id")
		change_column_null :participates, :wish, false
		add_index :participates, [:participant_id, :group_id, :wish], unique: true if !index_exists?(:participates, [:participant_id, :group_id, :wish])
  end
end
