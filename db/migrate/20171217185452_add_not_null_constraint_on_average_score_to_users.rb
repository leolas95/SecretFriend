class AddNotNullConstraintOnAverageScoreToUsers < ActiveRecord::Migration[5.1]
  def change
		change_column_null :users, :avgscore, false, 0.0
  end
end
