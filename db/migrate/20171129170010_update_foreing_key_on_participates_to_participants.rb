class UpdateForeingKeyOnParticipatesToParticipants < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :participates, :participants
		add_foreign_key :participates, :participants, on_delete: :cascade
  end
end
