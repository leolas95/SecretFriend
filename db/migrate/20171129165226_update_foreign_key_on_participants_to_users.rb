class UpdateForeignKeyOnParticipantsToUsers < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :participants, :users
		add_foreign_key :participants, :users, on_delete: :cascade
  end
end
