class CreateExchanges < ActiveRecord::Migration[5.1]
  def change
    create_table :exchanges do |t|
      t.date :exchange_date, null: false
      t.text :place, null: false
      t.references :group, foreign_key: true, null: false

      t.timestamps
    end
		add_index :exchanges, [:exchange_date, :group_id], unique: true
  end
end
