class CreateWishes < ActiveRecord::Migration[5.1]
  def change
    create_table :wishes do |t|
      t.references :participate, foreign_key: true, null: false
      t.string :participant_wish, null: false

      t.timestamps
    end
		add_index :wishes, [:participate_id, :participant_wish], unique: true
  end
end
