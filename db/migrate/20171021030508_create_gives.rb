class CreateGives < ActiveRecord::Migration[5.1]
  def change
    create_table :gives do |t|
			t.references :giver, null: false
			t.references :receiver, null: false
			t.references :exchange, foreign_key: true, null: false

      t.timestamps
    end
		add_index :gives, [:giver_id, :receiver_id, :exchange_id], unique: true
  end
end
