class UpdateForeignKeyOnGiftsToShops < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :gifts, :shops
		add_foreign_key :gifts, :shops, on_delete: :cascade
  end
end
