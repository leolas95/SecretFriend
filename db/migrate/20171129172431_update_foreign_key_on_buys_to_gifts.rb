class UpdateForeignKeyOnBuysToGifts < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :buys, :gifts
		add_foreign_key :buys, :gifts, on_delete: :cascade
  end
end
