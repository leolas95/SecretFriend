class UpdateForeignKeyOnBuysToParticipants < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :buys, :participants
		add_foreign_key :buys, :participants, on_delete: :cascade
  end
end
