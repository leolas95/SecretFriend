class CreateGifts < ActiveRecord::Migration[5.1]
  def change
    create_table :gifts do |t|
      t.string :code, null: false
      t.references :shop, foreign_key: true, null: false
      t.string :name, null: false
      t.float :price, null: false
      t.text :description

      t.timestamps
    end
		add_index :gifts, [:code, :shop_id], unique: true
  end
end
