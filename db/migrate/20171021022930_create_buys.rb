class CreateBuys < ActiveRecord::Migration[5.1]
  def change
    create_table :buys do |t|
      t.references :participant, foreign_key: true, null: false
      t.references :gift, foreign_key: true, null: false
      t.date :date_of_purchase, null: false

      t.timestamps
    end
		add_index :buys, [:participant_id, :gift_id], unique: true
  end
end
