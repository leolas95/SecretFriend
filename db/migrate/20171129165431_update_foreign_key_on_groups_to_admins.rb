class UpdateForeignKeyOnGroupsToAdmins < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :groups, :admins
		add_foreign_key :groups, :admins, on_delete: :cascade
  end
end
