class UpdateForeignKeyOnGivesToExchanges < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :gives, :exchanges
		add_foreign_key :gives, :exchanges, on_delete: :cascade
  end
end
