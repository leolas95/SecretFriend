class UpdateForeignKeyOnParticipatesToGroups < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :participates, :groups
		add_foreign_key :participates, :groups, on_delete: :cascade
  end
end
