class CreateLikes < ActiveRecord::Migration[5.1]
  def change
    create_table :likes do |t|
      t.references :user, foreign_key: true, null: false
      t.string :user_like, null: false

      t.timestamps
    end
		add_index :likes, [:user_id, :user_like], unique: true
  end
end
