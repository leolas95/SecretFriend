class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email, null: false
      t.string :name, null: false
      t.string :lastname, null: false
      t.integer :age, null: false
      t.string :gender
      t.float :avgscore

      t.timestamps
    end
		add_index :users, :email, unique: true
  end
end
