class CreateParticipates < ActiveRecord::Migration[5.1]
  def change
    create_table :participates do |t|
      t.references :participant, foreign_key: true, null: false
      t.references :group, foreign_key: true, null: false
      t.string :wish

      t.timestamps
    end
		add_index :participates, [:participant_id, :group_id], unique: true
  end
end
