class CreateAdmins < ActiveRecord::Migration[5.1]
  def change
    create_table :admins do |t|
      t.string :email, null: false
      t.references :user, foreign_key: true, null: false, unique: true

      t.timestamps
    end
		add_index :admins, :email, unique: true
  end
end
