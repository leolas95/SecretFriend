class AddGiverCalificationToGive < ActiveRecord::Migration[5.1]
  def change
    add_column :gives, :giver_calification, :float
  end
end
