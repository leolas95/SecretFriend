class UpdateForeignKeyOnWishesToParticipates < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :wishes, :participates
		add_foreign_key :wishes, :participates, on_delete: :cascade
  end
end
