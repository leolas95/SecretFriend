class UpdateForeignKeyOnAdminsToUsers < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :admins, :users
		add_foreign_key :admins, :users, on_delete: :cascade
  end
end
