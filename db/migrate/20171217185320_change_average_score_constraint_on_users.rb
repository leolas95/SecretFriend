class ChangeAverageScoreConstraintOnUsers < ActiveRecord::Migration[5.1]
  def change
		change_column_default :users, :avgscore, 0.0
  end
end
