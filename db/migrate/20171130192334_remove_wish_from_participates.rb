class RemoveWishFromParticipates < ActiveRecord::Migration[5.1]
  def change
		remove_column :participates, :wish, :string
  end
end
