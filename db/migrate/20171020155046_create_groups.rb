class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.string :name, null: false
      t.string :admin_email, null: false
      t.references :admin, foreign_key: true, null: false
      t.string :type, null: false
      t.integer :max_participants_allowed
      t.float :min_gift_price
      t.float :max_gift_price

      t.timestamps
    end
		add_index :groups, [:name, :admin_email], unique: true
  end
end
