class AddNullConstraintOnBirthdayColumnToUsers < ActiveRecord::Migration[5.1]
  def change
		change_column_null :users, :birthday, false
  end
end
