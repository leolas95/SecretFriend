class CreateParticipants < ActiveRecord::Migration[5.1]
  def change
    create_table :participants do |t|
      t.string :email, null: false
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
		add_index :participants, :email, unique: true
  end
end
