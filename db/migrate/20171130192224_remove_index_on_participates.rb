class RemoveIndexOnParticipates < ActiveRecord::Migration[5.1]
  def change
		remove_index :participates, name: "index_participates_on_participant_id_and_group_id_and_wish"
  end
end
