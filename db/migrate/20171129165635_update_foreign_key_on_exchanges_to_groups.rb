class UpdateForeignKeyOnExchangesToGroups < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :exchanges, :groups
		add_foreign_key :exchanges, :groups, on_delete: :cascade
  end
end
