class RemoveGroupTypeColumnFromGroups < ActiveRecord::Migration[5.1]
  def change
    remove_column :groups, :group_type, :string
  end
end
