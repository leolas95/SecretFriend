class ChangeMaxParticipantsConstraintOnGroup < ActiveRecord::Migration[5.1]
  def change
		change_column_null :groups, :max_participants_allowed, false
  end
end
