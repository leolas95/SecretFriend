class UpdateForeignKeyOnLikesToUsers < ActiveRecord::Migration[5.1]
  def change
		remove_foreign_key :likes, :users
		add_foreign_key :likes, :users, on_delete: :cascade
  end
end
