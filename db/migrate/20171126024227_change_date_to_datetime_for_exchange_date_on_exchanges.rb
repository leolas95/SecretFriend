class ChangeDateToDatetimeForExchangeDateOnExchanges < ActiveRecord::Migration[5.1]
  def change
		change_column :exchanges, :exchange_date, :datetime
  end
end
