# Relacion Grupo en el modelo logico
class Group < ApplicationRecord
  validates :name, presence: true
  validates :max_participants_allowed, presence: true
  validates :admin_email, presence: true
  validates :admin_id, presence: true
  belongs_to :admin

  has_many :participates, dependent: :destroy
  has_many :exchanges, dependent: :destroy

  # Retorna true si el grupo tiene espacio para otro participante, false si no
  def has_free_space
    self.participates.count < self.max_participants_allowed
  end

  # Retorna true si el grupo no tiene intercambios pendientes, false si no
  def doesnt_has_pending_exchanges
    Exchange.where(["group_id = ? and exchange_date >= ?", self.id, Time.now])
      .order(exchange_date: :desc)
      .empty?
  end
end
