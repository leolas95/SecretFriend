# Relacion Participante en el modelo logico
class Participant < ApplicationRecord
  validates :email, presence: true, uniqueness: true
  belongs_to :user
  has_many :participates, dependent: :destroy
end
