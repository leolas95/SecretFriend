# Relacion Tienda en el modelo logico
class Shop < ApplicationRecord
  validates :name, presence: true
  validates :address, presence: true, uniqueness: true
  has_many :gifts
end
