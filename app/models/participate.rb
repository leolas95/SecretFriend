# Relacion Participa en el modelo logico
class Participate < ApplicationRecord
  validates :participant_id, presence: true
  validates :group_id, presence: true
  belongs_to :participant
  belongs_to :group

  has_many :wishes, dependent: :destroy
  accepts_nested_attributes_for :wishes, allow_destroy: true, reject_if: :all_blank
end
