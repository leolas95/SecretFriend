# Relacion Gustos en el modelo logico
class Like < ApplicationRecord
  validates :user_id, presence: true
  validates :user_like, presence: true
  belongs_to :user
end
