# Relacion Regala en el modelo logico
class Give < ApplicationRecord
  validates :giver_id, presence: true
  validates :receiver_id, presence: true
  validates :exchange_id, presence: true
  belongs_to :giver, class_name: "User"
  belongs_to :receiver, class_name: "User"
end
