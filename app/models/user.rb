# Relacion Usuario en el modelo logico
class User < ApplicationRecord

  # Regex para validar el formato del email.
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true,
                    length: { maximum: 255 },
                    uniqueness: { case_sensitive: false }, # Al hacerlo asi, uniqueness = true
                    format: { with: VALID_EMAIL_REGEX }

  # Valida que el usuario tenga nombre, apellido y edad
  validates :name, presence: true
  validates :lastname, presence: true

  # Valida que el usuario tenga una password, con longitud minima 6 (arbitrario)
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  # El usuario solo puede estar una vez en la tabla admins
  has_one :admin, dependent: :destroy
  # El usuario solo puede estar una vez en la tabla participants
  has_one :participant, dependent: :destroy

  has_many :likes, dependent: :destroy
  accepts_nested_attributes_for :likes, allow_destroy: true

  # Antes de guardar un usuario en BD, convierte su email a minusculas
  before_save :downcase_email

  has_secure_password

  # Retorna el hash digest del string. Solo lo usamos para crear el password_digest
  # en fixtures/users.yml
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
      BCrypt::Engine.cost

    BCrypt::Password.create(string, cost: cost)
  end

  # Retorna true si el usuario es el admin del grupo, false si no
  def is_admin_of(group)
    self.email == group.admin_email
  end

  # Retorna true si el usuario es administrador de algun grupo, no importa cual. False si no
  def is_admin?
    Admin.exists? user_id: self.id
  end

  # Retorna true si el usuario es participante de algun grupo, no importa cual. False si no
  def is_participant?
    Participant.exists? user_id: self.id
  end

  # Retorna true si el usuario participa en el grupo, false si no
  def participates_in(group)
    self.participant && self.participant.participates && self.participant.participates.exists?(group_id: group.id, participant_id: self.participant.id)
  end

  # Retorna la edad del usuario
  def age
    now = Time.now.utc.to_date
    dob = self.birthday
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end

  private

  # Convierte en minusculas el email del usuario
  def downcase_email
    email.downcase!
  end
end
