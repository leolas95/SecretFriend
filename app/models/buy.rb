# Relacion Compra en el modelo logico
class Buy < ApplicationRecord
  validates :participant_id, presence: true
  validates :gift_id, presence: true
  belongs_to :participant
  belongs_to :gift
end
