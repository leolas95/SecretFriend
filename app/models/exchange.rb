# Relacion Intercambio en el modelo logico
class Exchange < ApplicationRecord
  validates :exchange_date, presence: true
  validates :group_id, presence: true
  validates :place, presence: true
  belongs_to :group

  has_many :give, dependent: :destroy
end
