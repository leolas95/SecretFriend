# Relacion Administrador en el modelo logico
class Admin < ApplicationRecord
  validates :email, presence: true, uniqueness: true
  belongs_to :user
  has_many :groups, dependent: :destroy
end
