# Relacion Regalo en el modelo logico
class Gift < ApplicationRecord
  validates :code, presence: true
  validates :name, :price, :description, presence: true
  belongs_to :shop
end
