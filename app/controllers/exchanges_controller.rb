class ExchangesController < ApplicationController
  $group = nil

  before_action :find_exchange, only: [:show, :update, :destroy]

  def new
    @exchange = Exchange.new
    $group = Group.find(params[:groupid])

    if $group.participates.count < 2
      flash[:danger] = "No hay suficientes participantes para realizar un intercambio"
      redirect_to $group and return
    end
  end

  def create
    @exchange = Exchange.new
    @exchange.exchange_date = params[:exchange][:exchange_date]
    @exchange.place = params[:exchange][:place]
    @exchange.group_id = $group.id

    if @exchange.save
      @party = make_exchange
      @party.each do |giver, receiver|
        g = Give.create(giver_id: User.find(giver).participant.id,
                        receiver_id: User.find(receiver).participant.id,
                        exchange_id: @exchange.id)

        UserMailer.new_exchange_email(User.find(giver), User.find(receiver), @exchange, $group).deliver_later
        RateGiverEmailJob.set(wait_until: @exchange.exchange_date + 1.day).perform_later(User.find(receiver), User.find(giver), @exchange, g)
      end

      flash[:success] = "Intercambio creado con exito"
      redirect_to $group
    else
      flash[:error] = "Error al crear intercambio"
      render "new"
    end
  end

  def show
  end

  def update
  end

  def destroy
    group = @exchange.group
    @exchange.destroy
    flash[:success] = "Intercambio eliminado con exito"
    redirect_to group
  end

  private

  # Retorna un hash con los ids de los usuarios que indica quien le regala
  # a quien. Las keys son los regaladores, y los values los recibidores`
  def make_exchange
    ActiveRecord::Base.connection.execute("call hacer_intercambio(#{$group.id}, @a, @b)")

    givers = []
    loop do
      ActiveRecord::Base.connection.execute("call hacer_intercambio(#{$group.id}, @a, @b)")
      givers = ActiveRecord::Base.connection.execute("select @a").first.first.split(" ").map(&:to_i)
      break if !givers.include?($group.admin_id)
    end

    receivers = ActiveRecord::Base.connection.execute("select @b").first.first.split(" ").map(&:to_i)

    # Tiene algun elemento true si hay al menos dos elementos iguales en la
    # misma posicion en givers y receivers
    # Ej:
    # givers    = [  1,     2,    3  ]
    # receivers = [  2,     1,    3  ]
    # iguales   = [false, false, true]
    iguales = givers.zip(receivers).map { |x, y| x == y }

    # Mientras haya un caso en que alquien se regale a si mismo vuelve a
    # calcular el arreglo de recibidores
    while iguales.include?(true)
      ActiveRecord::Base.connection.execute("call hacer_intercambio(#{$group.id}, @a, @b)")
      receivers = ActiveRecord::Base.connection.execute("select @b").first.first.split(" ").map(&:to_i)
      iguales = givers.zip(receivers).map { |x, y| x == y }
    end

    givers.zip(receivers).to_h
  end

  def find_exchange
    begin
      @exchange = Exchange.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:danger] = "El intercambio especificado no existe"
      redirect_to root_url and return
    end
  end
end
