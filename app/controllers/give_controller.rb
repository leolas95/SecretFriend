class GiveController < ApplicationController
  def new
    @give = Give.new
  end

  def edit
    @give = Give.find(params[:id])
  end

  def update
    @give = Give.find(params[:id])
    if @give.update_attributes(giver_calification: params[:give][:giver_calification])
      # Obten el usuario regalador y la cantidad de veces que ha participado
      user = @give.giver
      cant_veces_ha_participado = Give.where(giver_id: user)
      calification = @give.giver_calification

      # Calcula la nueva puntuacion promedio del usuario
      new_avgscore = (user.avgscore + calification) / cant_veces_ha_participado
      user.update_attributes(avgscore: new_avgscore)
      flash[:success] = "Calificacion realizada con exito! Gracias"
      redirect_to current_user and return
    else
      render "edit"
    end
  end
end
