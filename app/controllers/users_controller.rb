class UsersController < ApplicationController
  before_action :find_user, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:show, :index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]

  def home
  end

  def index
    @users = User.paginate(page: params[:page], per_page: 10)
  end

  def show
  end

  def new
    @user = User.new
    @user.likes.build
  end

  def create
    @user = User.new(user_params)

    if @user.save
      # Obtiene los gustos del usuario
      likes = params[:user][:likes_attributes].values

      # Inserta cada gusto en la bd
      likes.each do |like|
        Like.create(user_id: @user.id, user_like: like[:user_like])
      end

      log_in @user
      flash[:success] = "Registro realizado con exito. Bienvenido!"
      redirect_to @user and return
    else
      render "new"
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(user_edit_params)
      flash[:success] = "Datos actualizados con exito"
      redirect_to @user
    else
      render "edit"
    end
  end

  def destroy
    @user.destroy
    flash[:success] = "Cuenta eliminada con exito"
    redirect_to root_path
  end

  private

  def user_params
    params
      .require(:user)
      .permit(:name, :lastname, :email, :password, :password_confirmation, :birthday, :gender)
  end

  def user_edit_params
    params
      .require(:user)
      .permit(:name, :lastname, likes_attributes: [:id, :_destroy, :user_like])
  end

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      flash[:warning] = "Por favor, inicia sesion primero."
      redirect_to login_url
    end
  end

  def find_user
    begin
      @user = User.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:danger] = "El usuario no existe"
      redirect_to users_url and return
    end
  end

  # Retorna al root_path si el usuario actual no es aquel al que se quiere
  # editar
  def correct_user
    @user = User.find(params[:id])
    # Redirects to the home page if the current user is not the user whose
    # settings are trying to be edited.
    redirect_to(root_url) unless current_user?(@user)
  end
end
