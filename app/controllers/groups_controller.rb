class GroupsController < ApplicationController
  before_action :find_group, only: [:show, :edit, :update, :destroy]

  def index
    @groups = Group.all
  end

  def show
    @group = Group.find(params[:id])
    @participants = Participate.where group_id: @group.id
  end

  def new
    @group = Group.new
  end

  def edit
  end

  def create
    @group = Group.new(group_params)

    # Si el usuario existe...
    if User.find_by(email: current_user.email)

      # Y es administrador, entonces buscalo
      if Admin.find_by(email: current_user.email)
        admin = Admin.find_by(email: current_user.email)
        # Si no es administrador, crealo
      else
        admin = Admin.create(email: current_user.email,
                             user_id: current_user.id)
      end
      @group.admin_email = admin.email
      @group.admin_id = admin.id
    else
      # Esto no tiene sentido que pase (si el usuario no existe entonces como
      # podemos estar logueados?) pero por si acaso...
      flash[:danger] = "ERROR: el usuario #{current_user.name} no existe"
    end

    if @group.save
      flash[:success] = "Grupo creado con exito!"
      redirect_to @group and return
    else
      render "new"
    end
  end

  def update
  end

  def destroy
    @group.destroy
    redirect_to root_path
  end

  private

  def group_params
    params.require(:group).permit(:name, :max_participants_allowed,
                                  :min_gift_price, :max_gift_price)
  end

  def find_group
    begin
      @group = Group.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:danger] = "El grupo no existe"
      redirect_to groups_url and return
    end
  end
end
