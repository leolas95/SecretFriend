class ParticipatesController < ApplicationController
  def new
    @participate = Participate.new
    @group = Group.find(params[:groupid])
  end

  # Agrega un usuario a un grupo
  def create

    # Si el usuario no introdujo nada de nada
    if params[:participate][:wishes] == "Especificar deseos" &&
       params[:participate][:wishes_attributes].blank?
      flash.alert = "Los campos de deseo no pueden estar vacios. Por favor intentelo nuevamente"
      redirect_back(fallback_location: root_path) and return
    end

    # Si tenemos varios deseos
    if params[:participate].key?(:wishes_attributes)
      # Toma los deseos, rechazando los campos dejados en blanco
      wishes = params[:participate][:wishes_attributes]
        .values
        .reject { |key, val| key[:participant_wish].blank? }

      # Si al eliminar los campos en blanco no quedan deseos, es que
      # el usuario selecciono la opcion de varios deseos, pero no escribio nada
      if wishes.empty?
        flash.alert = "Los campos de deseo no pueden estar vacios. Por favor intentelo nuevamente"
        redirect_back(fallback_location: root_path) and return
      end
    else
      # Un solo string (Sorpresa)
      wishes = params[:participate][:wishes]
    end

    @group = Group.find(params[:groupid])
    current_participants_count = @group.participates.count

    # Verifica si el grupo esta full
    if current_participants_count >= @group.max_participants_allowed
      flash.alert = "El grupo esta lleno. No puedes entrar en este momento"
      redirect_to @group and return
    end

    if @group
      # Busca el participante, o lo crea si no existe
      @participant = Participant.where(email: current_user.email)
        .first_or_create(email: current_user.email,
                         user_id: current_user.id)

      # Busca la relacion entre el participante y el grupo, o la crea si no existe
      @participate = Participate.where(participant_id: @participant.id, group_id: @group.id)
        .first_or_create(participant_id: @participant.id, group_id: @group.id)

      if wishes.respond_to? :each
        wishes.each do |w|
          Wish.create(participate_id: @participate.id, participant_wish: (w[:participant_wish] unless w[:_destroy]))
        end
      else
        Wish.create(participate_id: @participate.id, participant_wish: wishes)
      end

      flash[:success] = "Ingresaste exitosamente al grupo!"
      redirect_to @group
    else
      render html: "El grupo no existe"
    end
  end

  def destroy
    participate = Participate.find(params[:id])
    cant_grupos_participando = participate.participant.participates.count

    # Si este era el ultimo grupo en el que participaba el usuario, eliminalo
    # de la tabla Participantes
    if (cant_grupos_participando == 1)
      Participant.find(participate.participant.id).destroy
    else
      Participate.find(params[:id]).destroy
    end
    flash[:success] = "Has abandonado el grupo exitosamente"
    redirect_to root_path
  end
end
