class ShopsController < ApplicationController
  before_action :find_shop, only: :show

  def index
    @shops = Shop.all
  end

  def show
  end

  private

  def find_shop
    begin
      @shop = Shop.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:danger] = "La tienda no existe"
      redirect_to shops_url and return
    end
  end
end
