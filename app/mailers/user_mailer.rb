class UserMailer < ApplicationMailer
  default from: "noreply@domain.com"

  # Envia el correo al usuario para notificarle de un nuevo intercambio
  def new_exchange_email(giver, receiver, exchange, group)
    @giver = giver
    @receiver = receiver
    @exchange = exchange
    @group = group
    mail(to: @giver.email, subject: "Se ha organizado un intercambio de amigo secreto")
  end

  # Envia el correo para recordarle al usuario que debe calificar el regalo recibido
  def rate_exchange_giver_email(user, giver, exchange, give)
    @user = user
    @giver = giver
    @exchange = exchange
    @give = give
    mail(to: @user.email, subject: "Califica tu regalo de amigo secreto")
  end
end
