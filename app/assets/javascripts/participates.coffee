# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# Para esconder uno de los campos al seleccionar el deseo para entrar al grupo
$(document).on 'turbolinks:load', ->
  $("#div_campos_varios_deseos").hide()
  $("#participate_wishes_sorpresa").click ->
    $("#div_campos_varios_deseos").hide()

  $("#participate_wishes_especificar_deseos").click ->
    $("#div_campos_varios_deseos").show()
