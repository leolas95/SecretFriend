module ApplicationHelper

  # Retorna el titulo completo segun la pagina visitada
  def full_title(page_title = "")
    base_title = "Amigo secreto"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
