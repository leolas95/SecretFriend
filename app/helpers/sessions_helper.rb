module SessionsHelper

  # Logea al usuario
  def log_in(user)
    # Coloca una cookie temporal en el navegador, con la id el usuario
    # encriptada
    session[:user_id] = user.id
  end

  # Retorna true si el usuario actual es el usuario especificado
  def current_user?(user)
    user == current_user
  end

  # Cierra la sesion del usuario actual
  def log_out
    session.delete :user_id
    @current_user = nil
  end

  # Retorna el usuario actual logueado, si es que hay uno
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def logged_in?
    !current_user.nil?
  end
end
