class RateGiverEmailJob < ApplicationJob
  queue_as :default

  # Se encarga de enviar el correo asincronamente a los participantes del grupo
  def perform(*args)
    user = args.first
    giver = args.second
    exchange = args.third
    give = args.fourth
    UserMailer.rate_exchange_giver_email(user, giver, exchange, give).deliver_later
  end
end
