require "test_helper"

class GroupTest < ActiveSupport::TestCase
  def setup
    @pedro = users(:pedro)
    @admin = Admin.create(email: @pedro.email, user_id: @pedro.id)
    @group = Group.create(name: "El grupo", admin_email: @admin.email,
                          admin_id: @admin.id,
                          max_participants_allowed: 10,
                          min_gift_price: 10.0,
                          max_gift_price: 50.0)
  end

  test "group should be valid" do
    assert @group.save
  end

  test "group name should be present" do
    @group.name = nil
    assert_not @group.valid?
    @group.name = ""
    assert_not @group.valid?
    @group.name = " "
    assert_not @group.valid?
  end

  test "admin_email should be present" do
    @group.admin_email = nil
    assert_not @group.valid?

    @group.admin_email = ""
    assert_not @group.valid?

    @group.admin_email = " "
    assert_not @group.valid?
  end

  test "admin_id should be present" do
    @group.admin_id = nil
    assert_not @group.valid?
  end

  test "max_participants_allowed should be positive" do
    if @group.max_participants_allowed
      assert_operator @group.max_participants_allowed, :>, 0
    end
  end

  test "max_participants_allowed should be greater than 1" do
    if @group.max_participants_allowed
      assert_operator @group.max_participants_allowed, :>=, 2
    end
  end

  test "min_gift_price should be positive" do
    if @group.min_gift_price
      assert_operator @group.min_gift_price, :>, 0
    end
  end

  test "max_gift_price should be positive" do
    if @group.max_gift_price
      assert_operator @group.max_gift_price, :>, 0
    end
  end
end
