require "test_helper"

class ExchangeTest < ActiveSupport::TestCase
  def setup
    @pedro = users(:pedro)
    @admin = Admin.create(email: @pedro.email, user_id: @pedro.id)
    @group = Group.create(name: "El grupo", admin_email: @admin.email,
                          admin_id: @admin.id,
                          max_participants_allowed: 10,
                          min_gift_price: 10.0,
                          max_gift_price: 50.0)

    @exchange = Exchange.new(exchange_date: Time.now, place: "El lugar",
                             group_id: @group.id)
  end

  test "exchange should be valid" do
    assert @exchange.save
  end

  test "exchange_date should be present" do
    @exchange.exchange_date = nil
    assert_not @exchange.valid?

    @exchange.exchange_date = ""
    assert_not @exchange.valid?

    @exchange.exchange_date = "   "
    assert_not @exchange.valid?
  end

  test "exchange place should be present" do
    @exchange.place = nil
    assert_not @exchange.valid?

    @exchange.place = ""
    assert_not @exchange.valid?

    @exchange.place = " "
    assert_not @exchange.valid?
  end

  test "group_id should be present" do
    @exchange.group_id = nil
    assert_not @exchange.valid?
  end
end
