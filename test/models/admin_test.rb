require "test_helper"

class AdminTest < ActiveSupport::TestCase
  def setup
    @pedro = users(:pedro)
    @admin = Admin.create(email: @pedro.email, user_id: @pedro.id)
  end

  test "admin should be valid" do
    assert @admin.save
  end

  test "email should be present" do
    @admin.email = " "
    assert_not @admin.valid?
  end

  test "user_id should be present" do
    @admin.user_id = nil
    assert_not @admin.valid?
  end
end
