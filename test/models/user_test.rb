require "test_helper"

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(email: "leo@gmail.com", name: "Leonardo", lastname: "Guedez",
                     password: "123456", password_confirmation: "123456", avgscore: 0.0, birthday: Time.now)
  end

  test "user should be valid" do
    assert @user.save
  end

  test "email should be present" do
    @user.email = " "
    assert_not @user.valid?
  end

  test "name should be present" do
    @user.name = " "
    assert_not @user.valid?
  end

  test "lastname should be present" do
    @user.lastname = " "
    assert_not @user.valid?
  end

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saves as lowercase" do
    mixed_case_email = "ejEMpLo@GMaiL.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " "
    assert_not @user.valid?
  end

  test "password should have a minimun length" do
    @user.password = @user.password_confirmation = "123"
    assert_not @user.valid?
  end
end
