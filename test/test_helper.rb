require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Retorna true si un usuario de prueba esta logueado, false sino.
  # Como el logged_in? de sessions_helper.rb no lo podemos usar en los
  # test, definimos este metodo que cumple con lo mismo
  def is_logged_in?
    !session[:user_id].nil?
  end
end
