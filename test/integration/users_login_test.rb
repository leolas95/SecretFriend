require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
	def setup
		@user = users(:pedro)
	end

	# Prueba que haya error al tratar de loguearse con datos invalidos
	test "login with invalid information" do
		# Ve a la pagina de login
		get login_path
		# Asegurate que se dibuje la vista new
		assert_template 'sessions/new'
		# POSTea a la pagina de login los campos
		post login_path, params: { session: { email: "", password: "" } }
		# Como tuvo que haber error, asegurate de que se vuelva a dibujar la vista new
		assert_template 'sessions/new'
		# Asegurate de que el flash no este vacio
		assert_not flash.empty?
		# Ve a la pagina principal
		get root_path
		# Asegurate que el flash ahora si este vacio (no se muestre otra vez)
		assert flash.empty?
	end

	# Prueba que haya exito al tratar de loguearse con datos validos
	test "login with valid information followed by logout" do
		get login_path
		post login_path, params: { session: { email: @user.email,
																					password: 'password'} }
		assert is_logged_in?
		assert_redirected_to @user
		follow_redirect!
		assert_template 'users/show'
		# Asegurate que no haya link para login (se supone que ya estamos logueados)
		assert_select "a[href=?]", login_path, count: 0
		# Pero asegurate que si haya link para cerrar sesion
		assert_select "a[href=?]", logout_path
		# Asegurate que haya un link para ir al perfil del usuario
		assert_select "a[href=?]", user_path(@user)

		# Cerramos sesion

		delete logout_path
		assert_not is_logged_in?
		assert_redirected_to root_url
		follow_redirect!
		# Asegurate que si haya link para login
		assert_select "a[href=?]", login_path
		# Pero asegurate que haya no link para cerrar sesion
		assert_select "a[href=?]", logout_path, count: 0
		# Asegurate que no haya link para ir al perfil del usuario (porque no estamos logueados)
		assert_select "a[href=?]", user_path(@user), count: 0
	end

end
