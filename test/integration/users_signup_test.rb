require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

	def setup
	end

	# Valida que un registro de usuario con datos invalidos no se guarda en la BD
	test "invalid signup information" do
		get new_user_path
		assert_no_difference 'User.count' do
			post users_path, params: { user: {
																					name: "",
																					lastname: "",
																					email: "invalido.com",
																					password: "123",
																					password_confirmation: "123",
																					birthday: 22.years.ago,
																					likes_attributes:
																					{
																						 "0" => { user_like: "Chocolate" },
																						 "1" => { user_like: "Musica" },
																						 "2" => { user_like: "Peliculas" }
																					}
																			}
																}
		end
		assert_template 'users/new'
	end

	# Valida un registro de usuario con datos validos
	test "valid signup information" do
		get new_user_path
		assert_difference 'User.count', 1 do
			post users_path, params: { user: {
																					name: "Leonardo",
																					lastname: "Guedez",
																					email: "leonardo@gmail.com",
																					password: "123456",
																					password_confirmation: "123456",
																					birthday: 22.years.ago,
																					likes_attributes:
																					{
																						"0" => { user_like: "Chocolate" },
																						"1" => { user_like: "Musica" },
																						"2" => { user_like: "Peliculas" }
																					}
																				}
															 }
		end
		follow_redirect!
		assert_template 'users/show'
		assert is_logged_in?
	end
end
