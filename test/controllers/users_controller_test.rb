require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "should get signup" do
    get new_user_path
    assert_response :success
  end

  test "should get login" do
    get login_path
    assert_response :success
  end
end
