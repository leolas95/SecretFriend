DROP PROCEDURE IF EXISTS hacer_intercambio;
DELIMITER $$
CREATE PROCEDURE hacer_intercambio(IN groupid INT, out a VARCHAR(200), out b VARCHAR(200))
BEGIN
  START TRANSACTION;
    DROP TABLE IF EXISTS temp1;
    DROP TABLE IF EXISTS temp2;
    -- Crea una tabla temporal para guardar los ids de los participantes
    create temporary table temp1 AS (SELECT users.id FROM
      users, participants, participates
      WHERE participates.participant_id = participants.id AND
            participants.user_id = users.id               AND
            participates.group_id = groupid);

    -- Crea otra tabla temporal para guardar los ids en orden aleatorio
    create temporary table temp2 AS (SELECT id FROM temp1 ORDER BY rand());

    SELECT cast(group_concat(id separator ' ') AS CHAR) INTO a FROM temp1;
    SELECT cast(group_concat(id separator ' ') AS CHAR) INTO b FROM temp2;
  COMMIT;
END$$
DELIMITER ;
