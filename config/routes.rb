Rails.application.routes.draw do
  root "pages#home"

  get "/perfil/:id", to: "users#show", as: "profile"

  get "participates/new/:groupid", to: "participates#new", as: "new_participate"
  post "participates/new/:groupid", to: "participates#create", as: "create_participate"
  delete "participates/:id", to: "participates#destroy", as: "leave_group"

  get "exchanges/new/:groupid", to: "exchanges#new", as: "new_exchange"

  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  delete "/logout", to: "sessions#destroy"

  get "groups/home"
  get "groups/index"

  get "shops/index"
  get "shops/show"

  resources :users
  resources :groups
  resources :exchanges
  resources :shops
  resources :give

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
