date_formats = {
	concise: '%d/%B/%Y'
}

Time::DATE_FORMATS.merge! date_formats
Date::DATE_FORMATS.merge! date_formats
