# Proyecto de Base de datos 2

Clona el proyecto
```
$ git clone https://gitlab.com/leolas95/SecretFriend
```

Instala las gems necesarias:
```
$ bundle install
```

Migra la base de datos:
```
$ rails db:migrate
```

Ubicate en la carpeta raiz del proyecto, y carga el procedimiento del intercambio:
```
$ bundle exec rails db < intercambio.sql
```

Llena la base de datos:
```
$ rails db:seed
```


Ejecuta los test para asegurarse de que todo este funcionando correctamente
```
$ rails test
```

Si todos los test pasaron, se puede probar la app en un servidor local
```
$ rails server
```

Se deberia poder acceder colocando en el navegador de internet 'localhost:3000'
